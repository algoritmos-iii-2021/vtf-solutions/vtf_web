import Vue from 'vue'
import VueRouter from 'vue-router'

import Home from './components/Home.vue'
import Login from './components/Login.vue'
import signup from './components/Signup.vue'
import inicio from './components/NavBar.vue'
import quest from './components/Quests.vue'
import publicar from './components/Publicar.vue'

Vue.use(VueRouter)

const routes = [
    { path: '/', component: Home },
    { path: '/login', component: Login},
    { path: '/signup', component: signup},
    { path: '/inicio', component: inicio},
    { path: '/quest', component: quest},
    { path: '/publicar', component: publicar},
]

export default new VueRouter({
    routes,
    mode: 'history'
})